import java.security.MessageDigest;

public class StringUtility {

    //Applies sha256 to a string and return the result

    public static String applysha256 (String hashdata) {

        try {
            MessageDigest msgd = MessageDigest.getInstance("SHA-256");
            //Apply SHA256 to our input
            byte[] hash = msgd.digest(hashdata.getBytes("UTF-8"));
            StringBuffer hexstring = new StringBuffer();
            for (int x = 0 ; x < hash.length ; x++ ){
                String hex = Integer.toHexString(0xff & hash[x]);
                if(hex.length()==1) hexstring.append('0');
                    hexstring.append(hex);
            }

            return hexstring.toString();
        }
        catch (Exception e){
            throw new RuntimeException(e);
        }
    }

}
