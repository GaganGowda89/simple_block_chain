import java.util.ArrayList;
import com.google.gson.GsonBuilder;
import org.jetbrains.annotations.NotNull;

public class Main {

    public static ArrayList<Block> blockChain  = new ArrayList<Block>();
    public static int difficult = 2;

    public static void main(String[] args) {


       // Block firstBlock = new Block("Hello this is the first block ", "0");
        blockChain.add(new Block("Hello this is the first block ", "0"));
        System.out.println("Mining Block 1");
        blockChain.get(0).mineBlock(difficult);

       // Block secondBlock = new Block("You are seeing the second block", firstBlock.hash);
        blockChain.add(new Block("Hello this is the second block ", blockChain.get(blockChain.size()-1).hash));
        System.out.println("Mining Block 2");
        blockChain.get(1).mineBlock(difficult);

       // Block thirdBlock = new Block("finally ur seeing the Third block", secondBlock.hash);
        blockChain.add(new Block("Hello this is the third block ", blockChain.get(blockChain.size()-1).hash));
        System.out.println("Mining Block 3");
        blockChain.get(2).mineBlock(difficult);

        System.out.println("\n Blockchain is Valid \n" + BlockchainValidation());

        String blockchainBuilder = new GsonBuilder().setPrettyPrinting().create().toJson(blockChain);
        System.out.println("\n The BlockChain is : \n");
        System.out.println(blockchainBuilder);
    }

    //Any changes in this method will cause this method to return false
    @NotNull
    public static Boolean BlockchainValidation(){

         Block current_Block;
         Block previous_Block;
         String hashtarget = new String(new char [difficult]).replace("\0", "0");
         System.out.println("Just checking what needs to be the output to be mined : "+ hashtarget);

         for(int i=1; i< blockChain.size(); i++) {

             current_Block  = blockChain.get(i);
             previous_Block = blockChain.get(i-1);

             //compare registered hash and calculated hash:
             if(!current_Block.hash.equals(current_Block.calculateHash()))
             {
                 System.out.println("Current hashes not equal");
                 return false;
             }
             //compare previous hash and previous registered hash:
             if(!previous_Block.hash.equals(current_Block.previoushash))
             {
                 System.out.println("Previous block not equal to current one");
                 return false;
             }

             //check if hash is solved
             if(!current_Block.hash.substring(0,difficult).equals(hashtarget))
                 System.out.println("This block hasn't been mined");
                 return false;
         }
        return true;
    }
}
