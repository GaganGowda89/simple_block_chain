import java.util.Date;

public class Block {

    public String hash;
    public String previoushash;
    private String data;  // data is a simple message
    private long timestamp;     //date as of 1/1/1970 in milliseconds
    private int once;

    // Block Constructor
    public Block(String data, String previoushash)
    {
        this.data = data;
        this.previoushash = previoushash;
        this.timestamp = new Date().getTime();
        this.hash = calculateHash();
    }

    //Calculate new hash based on blocks contents
    public String calculateHash(){
        String computedHash = StringUtility.applysha256(
                previoushash +
                        Long.toString(timestamp)+
                        Integer.toString(once) +
                        data
        );
        return computedHash;
    }

    //Mining the block using mineBlock function with difficulty level (df)
    public void mineBlock (int df){
        String target =  new String(new char[df]).replace('\0', '0');
        while (!hash.substring(0, df).equals(target)) {
            once++;
            hash = calculateHash();
        }
        System.out.println("Block Mined !!! :" + hash);
    }
}
